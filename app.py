from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from dbconfig import mysql_settings
import datetime
from uuid import uuid4
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://{}:{}@{}/{}'.format(mysql_settings["username"],
                                                                     mysql_settings["password"],
                                                                     mysql_settings["server"],
                                                                     mysql_settings["database"])


app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

from models.task_model import DD

db.create_all()
db.session.commit()

@app.route('/')
def home():
    tasks = DD.query.all()
    print(tasks[0].__dict__)
    print(tasks)
    return render_template('index.html', tasks= tasks)

@app.route('/create-task', methods=['POST'])
def create():
    if request.method == 'POST':
        try:
            #read data from post man as json
            context = request.get_json(force=True)
            task = DD(content=context["content"], done=context["done"], do=False)
        except Exception as ex:
            print("#########")
            print(ex)
            #if data returned from a form
            content = request.form["content"]
            task = DD(content=content, done=False)
            pass
        db.session.add(task)
        db.session.commit()
        print("********")
        print(task)
        return redirect(url_for('home'))

@app.route('/done/<id>')
def done(id):
    task = DD.query.filter_by(id=int(id)).first()
    task.done = not task.done
    db.session.commit()
    return redirect(url_for('home'))

@app.route('/update/<id>')
def update(id):
    task = DD.query.filter_by(id=int(id)).first()
    task.content = "update data 1"
    db.session.commit()

    print(task._asdict())
    return task._asdict()

@app.route('/delete/<id>')
def delete(id):
    task = DD.query.filter_by(id=int(id)).delete()
    db.session.commit()
    return redirect(url_for('home'))


if __name__ == '__main__':
    app.debug = True
    app.run(port=5001)
